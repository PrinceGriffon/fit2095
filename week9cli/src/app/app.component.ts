import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  data = [];
  name = "";
  due = "";
  status = "";
  desc ="";
  count= 0;

  newItem() {
    this.data.push({name: this.name, due: this.due, status: this.status, desc: this.desc});
    if (this.data[this.data.length - 1].status === 'completed') {
    this.count++;
  }
  }
  clearItems() {
    for (let x = 0; x < this.data.length; x++) {
      if (this.data[x].status === 'completed')
        this.data.splice(x,1);

      }
    }
    deleteItem( i) {
     this.data.splice(i,1);
    }
  }

